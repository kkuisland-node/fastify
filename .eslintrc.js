module.exports = {
   env: {
      commonjs: true,
      es2021: true,
      node: true,
   },
   extends: 'eslint:recommended',
   parserOptions: {
      ecmaVersion: 12,
   },
   rules: {
      'no-console': 0,
      'callback-return': 2,
      'global-require': 0, // 최상위 모듈 범위에서 요구 사항 적용
      'handle-callback-err': 2, // 콜백에서 오류 처리 강제
      'no-mixed-requires': 2, // 정규 변수 혼합을 허용하지 않으며 선언 필요
      'no-new-require': 2, // 필요 함수와 함께 새 연산자를 사용할 수 없음
      'no-path-concat': 2, // __sysname 및 __sysis와의 문자열 연결 허용 안 함
      'no-process-exit': 2, // process.vmdk 허용 안 함
      'no-restricted-imports': 0, // 지정된 노드 가져오기 사용 제한
      'no-restricted-modules': 0, // 지정된 노드 모듈의 사용 제한
      'no-sync': 1, // 동기식 메서드를 사용할 수 없음

      //스타일 문제
      // 'space-in-brackets': [ 2, 'always' ], // 블록안에 공백
      'array-bracket-spacing': [ 2, 'always' ], // 배열 대괄호 내부 간격 적용
      'block-spacing': [ 2, 'always' ], // 단일 선 블록 내부의 공백 허용 또는 적용
      'brace-style': 2, // 하나의 실제 가새 스타일 적용
      camelcase: 1, // 카멜 대소문자 이름 필요
      'comma-spacing': [
         2,
         {
            before: false,
            after: true,
         },
      ], // 쉼표 앞과 뒤에 공백 적용
      'comma-style': 2, // 하나의 실제 쉼표 스타일 적용
      'computed-property-spacing': [ 2, 'always' ], // 계산된 속성 내에서 패딩 필요 또는 허용 안 함
      'consistent-this': 2, // 현재 실행 컨텍스트를 캡처할 때 일관된 이름 지정 적용
      'eol-last': 2, // 빈 줄이 여러 개 없는 파일 끝에 새 줄 적용
      'func-names': 0, // 함수 식에 이름이 있어야 함
      'func-style': 0, // 함수 선언 또는 표현식 사용 강제
      'id-blacklist': 0, // 특정 식별자가 사용되지 않도록 블랙리스트 지정
      'id-length': 0, // 이 옵션은 최소 및 최대 식별자 길이(예: 이름, 속성 이름)를 적용합니다.
      'id-match': 0, // 제공된 정규식과 일치하도록 식별자 필요
      indent: [ 'error', 3 ], // 코드에 대한 탭 또는 공백 너비 지정
      'jsx-quotes': 0, // JSX 특성에 큰따옴표를 사용할지 작은따옴표를 사용할지 지정
      'key-spacing': 2, // 객체 리터럴 속성에서 키와 값 사이의 간격 적용
      'keyword-spacing': [
         2,
         {
            before: true,
            after: true,
            overrides: {
               if: { after: true },
               for: { after: true },
               while: { after: true },
               catch: { after: true },
            },
         },
      ], // 키워드 앞과 뒤에 공백 적용
      'linebreak-style': 0, // 'LF'와 'CRLF'의 혼합을 줄 바꿈으로 허용하지 않음
      'lines-around-comment': 0, // 주석 주위에 빈 줄을 붙이다
      'max-depth': 1, // 블록을 내포할 수 있는 최대 깊이 지정
      'max-len': [ 1, 200 ], // 프로그램에서 줄의 최대 길이 지정
      'max-lines': 0, // 최대 파일 길이를 적용하다
      'max-nested-callbacks': 2, // 내포할 수 있는 최대 깊이 콜백 지정
      'max-params': 0, // 에서는 함수 선언에 사용할 수 있는 파라미터 수를 제한합니다.
      'max-statements': 0, // 함수에 허용되는 최대 문 수를 지정합니다.
      'max-statements-per-line': 0, // 한 줄에 허용되는 최대 문 수 시행합니다.
      'new-cap': 0, // 건설업자에게 대문자를 요구하다.
      'new-parens': 2, // 인수 없이 생성자를 호출할 때 괄호 생략 허용
      'newline-after-var': 0, // 변수 선언 후 빈 새 줄을 요구하거나 허용하지 않습니다.
      'newline-before-return': 0, // 반송 전 새 행이 필요합니다.
      'newline-per-chained-call': [ 'error', { ignoreChainWithDepth: 2 } ], // 통화 체인을 할 때 각 통화 후 새 회선을 적용합니다.
      'no-array-constructor': 2, // 배열 생성자를 사용할 수 없음
      'no-bitwise': 0, // 비트 연산자 사용 안 함
      'no-continue': 0, // 계속 문 사용을 허용하지 않습니다.
      'no-inline-comments': 0, // 코드 뒤에 인라인 주석을 허용하지 않습니다.
      'no-lonely-if': 2, // 다른 블록의 유일한 문인 경우 허용하지 않음
      'no-mixed-operators': 0, // 서로 다른 연산자의 혼합을 허용하지 않습니다.
      'no-mixed-spaces-and-tabs': 2, // 혼합된 공백 및 탭 들여쓰기 허용 안 함
      'no-multiple-empty-lines': 2, // 여러 줄의 빈 줄을 허용하지 않습니다.
      'no-negated-condition': 0, // 부정된 조건을 허용하지 않다
      'no-nested-ternary': 0, // 중첩된 3진수 식을 허용하지 않음
      'no-new-object': 2, // 개체 생성자 사용을 허용하지 않음
      'no-plusplus': 0, // 단항 연산자 사용을 허용하지 않음 및 --
      'no-restricted-syntax': 0, // 코드에서 특정 구문 사용을 허용하지 않습니다.
      'no-spaced-func': 2, // 함수 식별자와 응용 프로그램 사이에 공백 허용 안 함
      'no-ternary': 0, // 3진수 연산자 사용 금지
      'no-trailing-spaces': 2, // 줄 끝에서 후행 공백 허용 안 함
      'no-underscore-dangle': 0, // 식별자에서 밑줄 달기 허용 안 함
      'no-unneeded-ternary': 2, // 더 간단한 대안이 존재하는 경우 3차 연산자를 사용할 수 없음
      'no-whitespace-before-property': 2, // 속성 앞에 공백 허용 안 함
      'object-curly-newline': 0, // 가새 내부에서 일관된 줄 바꿈 적용
      'object-curly-spacing': [ 2, 'always' ], // 곱슬 가새 안에 패딩 필요 또는 허용 안 함
      'object-property-newline': 0, // 객체 특성을 별도의 줄에 배치 강제
      'one-var': [ 2, 'never' ], // 함수당 하나의 변수 선언 필요 또는 허용 안 함
      'one-var-declaration-per-line': 2, // 변수 선언 주위에 새 줄 필요 또는 허용 안 함
      'operator-assignment': 0, // 가능한 경우 할당 연산자 속기 요구 또는 전체 금지
      'operator-linebreak': [ 1, 'before' ], // 줄 바꿈 전후에 연산자를 배치하도록 강제합니다.
      'padded-blocks': [ 2, 'never' ], // 블록 내에서 패딩 적용
      'quote-props': [ 2, 'as-needed' ], // 객체 리터럴 속성 이름 주위에 따옴표 필요
      quotes: [ 2, 'single' ], // 백틱, 큰따옴표 또는 작은따옴표를 사용할지 지정
      'require-jsdoc': 0, // Requires JSDoc how
      'semi-spacing': 2, // 세미콜론 앞과 뒤에 공백 적용
      'sort-imports': 0, // 모듈 내에서 가져오기 선언 정렬
      semi: [ 2, 'never' ], // ASI 대신 세미콜론 사용 필요 또는 금지
      'sort-vars': 0, // 동일한 선언 블록 내에서 변수 정렬
      'space-before-blocks': 2, // 블록 앞에 공백 필요 또는 허용 안 함
      'space-before-function-paren': [ 2, 'never' ], // 함수가 괄호를 열기 전에 공백 필요 또는 허용 안 함
      'space-in-parens': 2, // 괄호 안의 공백 필요 또는 허용 안 함
      'space-infix-ops': 2, // 연산자 주위에 공백 필요
      'space-unary-ops': 2, // 단항 연산자 앞/뒤에 공백 필요 또는 허용 안 함
      'spaced-comment': 0, // 주석의 // 또는 /* 바로 뒤에 공백 필요 또는 허용 안 함
      'unicode-bom': 0, // 유니코드 BOM 필요 또는 허용 안 함
      'wrap-regex': 0, // 정규식 리터럴을 괄호로 묶어야 함
      'arrow-parens': [ 2, 'always' ]
   },
}
