'use strict'

const fp = require('fastify-plugin')

/**
 * 쿠키 읽기 및 설정에 대한 지원을 추가하는 Fastify 용 플러그인 .
 * 이 플러그인의 쿠키 구문 분석은 Fastify의 onRequest후크 를 통해 작동합니다 .
 * 따라서 onRequest이 플러그인의 동작에 의존 하는 다른 후크 보다 먼저 등록해야 합니다 .
 * fastify-cookie v2.x 는 Fastify@1과 Fastify@2를 모두 지원합니다. fastify-cookiev3는 Fastify@2만 지원합니다.
 *
 * @see https://github.com/fastify/fastify-cookie
 */

module.exports = fp(async function(fastify, opts, next) {
   fastify.register(require('fastify-cookie'), {
      secret: process.env.COOKIE_SIGN_KEY,
   })
   next()
})
