'use strict'

const fp = require('fastify-plugin')

/**
 * Fastify용 JWT 유틸리티는 내부적으로 jsonwebtoken 을 사용 합니다 .
 * fastify-jwtFastify@3를 지원합니다. fastify-jwt v1.x 는 Fastify@2를 모두 지원합니다.
 *
 * @see https://github.com/fastify/fastify-jwt
 */
module.exports = fp(async function(fastify, opts, next) {
   fastify.register(require('fastify-jwt'), {
      secret: process.env.JWT_SECRET_KEY,
      decode: { complete: true },
      sign: {
         algorithm: 'HS256',
         issuer: 'kkuisland.com',
         expiresIn: '7d',
         subject: 'kkuToken',
      },
      verify: {
         algorithms: 'HS256',
         issuer: 'kkuisland.com',
         maxAge: '7d',
         subject: 'kkuToken',
      },
      cookie: {
         cookieName: 'kkuToken',
         signed: true,
      },
   })
   next()
})
